package com.cognizant.retailamate.jda.buddystore.popup;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.cognizant.retailamate.jda.R;



public class FulfillRequestPopUp extends AppCompatActivity {
    private static final int RESULT_CLOSE_ALL = 0;

/*    ClearInterface buddyStoreDetailScreen;
    ClearInterface buddyStoreSelectionScreen;
    ClearInterface rmProductScreen;
    ClearInterface requestModePopUp;

    public FulfillRequestPopUp(BuddyStoreDetailScreen buddyStoreDetailScreen) {
        this.buddyStoreDetailScreen = buddyStoreDetailScreen;
    }

    public FulfillRequestPopUp(BuddyStoreSelectionScreen buddyStoreSelectionScreen) {
        this.buddyStoreDetailScreen = buddyStoreSelectionScreen;
    }

    public FulfillRequestPopUp(RMProductScreen rmProductScreen) {
        this.buddyStoreDetailScreen = rmProductScreen;
    }

    public FulfillRequestPopUp(RequestModePopUp requestModePopUp) {
        this.buddyStoreDetailScreen = requestModePopUp;
    }*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fulfill_request_pop_up);
    }

    public void clearStack(View view) {
        setResult(RESULT_CLOSE_ALL);
        finish();

       /* buddyStoreDetailScreen.clearActivity();
        buddyStoreSelectionScreen.clearActivity();
        rmProductScreen.clearActivity();
        requestModePopUp.clearActivity();*/
    }
}
