package com.cognizant.retailamate.jda.associate;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import com.cognizant.retailamate.jda.Adapter.AssociateJobAdapterold;
import com.cognizant.retailamate.jda.Model.AssociateJobModelold;
import com.cognizant.retailamate.jda.R;
import com.cognizant.retailamate.jda.utils.Constants;
import com.cognizant.retailamate.jda.Network.VolleyHelper;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 452781 on 11/28/2016.
 */
public class AssociateJobListActivityold extends AppCompatActivity {

    private List<AssociateJobModelold.ResourceBean> jobList = new ArrayList<>();
    private RecyclerView recyclerView;
    private AssociateJobAdapterold mAdapter;
    Gson gson;
    ProgressDialog progress;

    AssociateJobModelold associateJobModelold;
//    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.associate_job_list);

        Toolbar mToolBar = (Toolbar) findViewById(R.id.toolbar);
        mToolBar.setNavigationIcon(R.drawable.back);
        mToolBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        gson = new Gson();
        setJobData();

        recyclerView = (RecyclerView) findViewById(R.id.associate_job_recyc);

        mAdapter = new AssociateJobAdapterold(getApplicationContext(), jobList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();

    }

    private void setJobData() {

        progress = ProgressDialog.show(AssociateJobListActivityold.this, "Connecting",
                "Please wait...", true);


        StringRequest stringRequest = new StringRequest(Request.Method.GET, Constants.getAssociateJobURL + 2,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
//                        Toast.makeText(AssociateProfileActivity.this, s, Toast.LENGTH_LONG).show();
                        progress.dismiss();
                        associateJobModelold = gson.fromJson(s, AssociateJobModelold.class);
/*
                        AssociateJobModelold.ResourceBean resourceBean=new AssociateJobModelold.ResourceBean();
                        resourceBean.setItemName("Order Picking");
                        resourceBean.setLocation("#1243");
                        resourceBean.setType("Products");
                        resourceBean.setQuantity(1);
                        jobList.add(resourceBean);*/
                        for (int i = 0; i < associateJobModelold.getResource().size(); i++) {
                            jobList.add(associateJobModelold.getResource().get(i));
                        }
                        recyclerView = (RecyclerView) findViewById(R.id.associate_job_recyc);

                        mAdapter = new AssociateJobAdapterold(getApplicationContext(), jobList);
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                        recyclerView.setLayoutManager(mLayoutManager);
                        recyclerView.setItemAnimator(new DefaultItemAnimator());
                        recyclerView.setAdapter(mAdapter);

                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Dismissing the progress dialog
                        progress.dismiss();

                        Toast.makeText(AssociateJobListActivityold.this, "Login failed. Please enter valid credentials.", Toast.LENGTH_LONG).show();
                        Log.e("@@##", "VolleyError = " + error);
                        Log.e("@@##", "VolleyError = " + error.getMessage());

                        Log.e("@@##", "TEST ");

                    }

                });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES * 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyHelper.getInstance(AssociateJobListActivityold.this).addToRequestQueue(stringRequest);


    }
}