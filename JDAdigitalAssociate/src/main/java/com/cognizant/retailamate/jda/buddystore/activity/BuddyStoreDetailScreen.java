package com.cognizant.retailamate.jda.buddystore.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;


import com.cognizant.retailamate.jda.R;
import com.cognizant.retailamate.jda.buddystore.adapter.ProductAdapter;
import com.cognizant.retailamate.jda.buddystore.interfaces.ProductInterface;
import com.cognizant.retailamate.jda.buddystore.interfaces.ClearInterface;
import com.cognizant.retailamate.jda.buddystore.model.Product;
import com.cognizant.retailamate.jda.buddystore.popup.FulfillRequestPopUp;
import com.cognizant.retailamate.jda.utils.Constants;
import com.google.gson.Gson;

import java.util.ArrayList;

public class BuddyStoreDetailScreen extends AppCompatActivity implements View.OnClickListener, ProductInterface, ClearInterface {

    int storeElement;
    private static final int RESULT_CLOSE_ALL = 0;

    private Gson gson;
    public static Product product;
    private ArrayList<Product.VariantsBean> mProductArrayList;
    private RecyclerView mRecyclerView;
    private ProductAdapter mAdapter;
    private Context mContext;
    Button fulfillRequestButton;
    TextView storeName, productName, productIdtv, productCategory;

    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.buddy_store_detail_screen);


        int position = getIntent().getIntExtra("position", 0);

        storeName = (TextView) findViewById(R.id.store_name);
        productName = (TextView) findViewById(R.id.product_name);
        productIdtv = (TextView) findViewById(R.id.product_id);
        productCategory = (TextView) findViewById(R.id.product_category);
        imageView = (ImageView) findViewById(R.id.image_view);

        imageView.setImageResource(R.drawable.ic_shop);

        storeElement = getIntent().getIntExtra("store", -1);

        fulfillRequestButton = (Button) findViewById(R.id.fulfill_request_button);

        fulfillRequestButton.setOnClickListener(this);

        storeName = (TextView) findViewById(R.id.store_name);


        mContext = getApplicationContext();

        gson = new Gson();

        mProductArrayList = new ArrayList<>();

        String jsonString = "buddystoreresponse_" + Constants.productId;


        product = gson.fromJson(Constants.loadJSONFromAsset(jsonString, mContext), Product.class);


        storeName.setText(BuddyStoreSelectionScreen.store.getStores().get(position).getName());
        productName.setText(product.getName());
        productIdtv.setText(product.getID());
        productCategory.setText(product.getCategory());

        for (int i = 0; i < product.getVariants().size(); i++) {
            mProductArrayList.add(product.getVariants().get(i));
        }

        mRecyclerView = (RecyclerView) findViewById(R.id.product_info_recycler_view);

        mAdapter = new ProductAdapter(mProductArrayList, mContext, this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mAdapter);


    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fulfill_request_button:
                Intent intent = new Intent(mContext, FulfillRequestPopUp.class);
                startActivityForResult(intent,RESULT_CLOSE_ALL);
                break;
        }
    }

    @Override
    public void enableButton() {
        fulfillRequestButton.setEnabled(true);
    }

    @Override
    public void disableButton() {
        fulfillRequestButton.setEnabled(false);
    }

    @Override
    public void setIfDisabled() {
        if (!fulfillRequestButton.isEnabled()) {
            fulfillRequestButton.setEnabled(true);
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch(resultCode)
        {
            case RESULT_CLOSE_ALL:
                setResult(RESULT_CLOSE_ALL);
                finish();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
    @Override
    public void clearActivity() {
        finish();
    }
}
