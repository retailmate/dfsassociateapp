package com.cognizant.retailamate.jda.activity;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.NotificationCompat;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.Switch;

import com.cognizant.retailamate.jda.R;
import com.cognizant.retailamate.jda.Service.AssociateSignalRService;
import com.cognizant.retailamate.jda.utils.AccountState;

/**
 * Created by 452781 on 11/28/2016.
 */
public class SettingsActivity extends AppCompatActivity {

    Toolbar toolbar;

    private static final String TAG = SettingsActivity.class.getSimpleName();

    SwitchCompat switch1;

    RelativeLayout offlinelayout;

    AppCompatButton offlinealert, offlinenotification;

    String userRole;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        userRole = getIntent().getStringExtra("userrole");


        switch1 = (SwitchCompat) findViewById(R.id.switch1);
        offlinelayout = (RelativeLayout) findViewById(R.id.offlinelayout);

        offlinealert = (AppCompatButton) findViewById(R.id.offlinealert);
        offlinenotification = (AppCompatButton) findViewById(R.id.offlinenotification);


        switch1.setChecked(AccountState.getOfflineMode());

        if (AccountState.getOfflineMode() && userRole.equals("associate")) {
            offlinelayout.setVisibility(View.VISIBLE);
        }

        switch1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    switch1.setChecked(true);
                    AccountState.setOfflineMode(true);
                    if (userRole.equals("associate")) {
                        offlinelayout.setVisibility(View.VISIBLE);
                    }
                } else {
                    switch1.setChecked(false);
                    AccountState.setOfflineMode(false);
                    offlinelayout.setVisibility(View.INVISIBLE);
                }
            }
        });


        offlinealert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                showAlertDialog();

            }
        });
        offlinenotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
                sendNotification();

            }
        });

    }


    public boolean onCreateOptionsMenu(Menu menu) {


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // toggle nav drawer on selecting action bar app icon/title

        // Handle action bar actions click
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }
    }


    /*
   Create alert window
    */
    public void showAlertDialog() {

        Intent intent = new Intent(getApplicationContext(), DialogActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);


    }

    /*

    create offline system notification
     */
    public void sendNotification() {

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this);

//Create the intent that’ll fire when the user taps the notification//

        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

        mBuilder.setContentIntent(pendingIntent);

        mBuilder.setSmallIcon(R.mipmap.retailmate_appicon);
        mBuilder.setContentTitle("New Tasks Alert");
        mBuilder.setContentText("You have been assigned new tasks, please login to check!");

        NotificationManager mNotificationManager =

                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        mNotificationManager.notify(001, mBuilder.build());
    }
}
