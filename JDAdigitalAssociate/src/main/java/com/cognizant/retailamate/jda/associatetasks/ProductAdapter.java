package com.cognizant.retailamate.jda.associatetasks;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.http.AndroidHttpClient;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpClientStack;
import com.android.volley.toolbox.HttpStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.cognizant.retailamate.jda.R;
import com.cognizant.retailamate.jda.orderdelivery.ScanActivity;
import com.cognizant.retailamate.jda.utils.AccountState;
import com.cognizant.retailamate.jda.utils.Constants;
import com.google.gson.Gson;

import java.io.Serializable;
import java.net.HttpURLConnection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 599540 on 11/15/2016.
 */

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.productViewholder> {
    List<AssociateJobModel.ResourceBean> dispdata = Collections.emptyList();
    public Context context;
    int count;
    int c = 0;
    private final LayoutInflater inflater;

    RequestQueue mRequestQueue;


//   private final int VIEW_TYPE_EXTRA=1;
//    private final int VIEW_TYPE_LIST=0;

    public ProductAdapter(Context context, List<AssociateJobModel.ResourceBean> dispdata) {
        inflater = LayoutInflater.from(context);
        this.dispdata = dispdata;
        this.context = context;
    }

    @Override
    public productViewholder onCreateViewHolder(ViewGroup parent, int viewType) {


        View view = inflater.inflate(R.layout.productview_layout, parent, false);
        return new productViewholder(view);

    }

    @Override
    public void onBindViewHolder(final productViewholder holder, final int position) {


        if (position < dispdata.size()) {
            final AssociateJobModel.ResourceBean current = dispdata.get(position);
            holder.pname.setText(current.getItemName());
            holder.taskType.setText(current.getType());
            holder.check.setClickable(true);
            holder.check.setOnCheckedChangeListener(null);
            holder.check.setChecked(current.isselected());
            holder.more.setVisibility(View.VISIBLE);
            holder.more.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, DetailsFragment.class);
                    Gson gson = new Gson();
                    intent.putExtra("productDetails", gson.toJson(current));

                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
//                    Toast.makeText(context, current.getItemName(), Toast.LENGTH_SHORT).show();
                }
            });
            holder.barcode.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    Toast.makeText(context, "Barcode", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(context, ScanActivity.class);

                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("barcodeid", current.getItemNum());
                    intent.putExtra("position", position);


                    context.startActivity(intent);


                }
            });

            if (current.isselected()) {
                holder.productwithcheckbox_item.setBackgroundResource(R.drawable.itemselected);
            } else {
                holder.productwithcheckbox_item.setBackgroundResource(R.color.lightestgrey);
            }
            holder.check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                                        @Override
                                                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                                            current.setIsselected(isChecked);
                                                            if (buttonView.isChecked()) {
                                                                if (!AccountState.getOfflineMode()) {
                                                                    UpdateOrderTask(String.valueOf(current.getId()), "closed");
                                                                }
                                                                holder.productwithcheckbox_item.setBackgroundResource(R.drawable.itemselected);
                                                                holder.more.setImageResource(R.drawable.more_inact_crl);
                                                                holder.taskType.setTypeface(null, Typeface.BOLD);
                                                                holder.taskType.setTextColor(ContextCompat.getColor(context, R.color.darkGrey));
                                                            } else {
                                                                if (!AccountState.getOfflineMode()) {
                                                                    UpdateOrderTask(String.valueOf(current.getId()), "open");
                                                                }
                                                                holder.productwithcheckbox_item.setBackgroundResource(R.color.lightestgrey);
                                                                holder.more.setImageResource(R.drawable.more_act_crl);
                                                                holder.taskType.setTypeface(null, Typeface.NORMAL);
                                                                holder.taskType.setTextColor(ContextCompat.getColor(context, R.color.grey));
                                                            }
                                                            c = checkcount(isChecked);
                                                            if (c == dispdata.size()) {
                                                                OrderDetails.button.setEnabled(true);
                                                                OrderDetails.button.setBackgroundResource(R.drawable.round_green);
                                                                OrderDetails.button.setText("Order Completed");
                                                                OrderDetails.button.setPadding(50, 0, 50, 0);
//                         holder.check.setVisibility(View.VISIBLE);
                                                            } else {
                                                                OrderDetails.button.setEnabled(false);
                                                                OrderDetails.button.setBackgroundResource(R.drawable.round_darkgrey);
                                                                OrderDetails.button.setText("Order Pending");
                                                                OrderDetails.button.setPadding(50, 0, 50, 0);
//                          holder.check.setVisibility(View.INVISIBLE);
                                                            }
//                     Toast.makeText(context,"Checked"+c,Toast.LENGTH_SHORT).show();
                                                        }
                                                    }

            );
            //}
        }
    }

    public int checkcount(boolean ischecked) {
        count += ischecked ? 1 : -1;
        return count;
    }

    @Override
    public int getItemCount() {
        return (dispdata == null) ? 0 : dispdata.size();
    }

    @Override
    public int getItemViewType(int position) {
//        return (position==dispdata.size())? VIEW_TYPE_EXTRA:VIEW_TYPE_LIST;
        return position;
    }

    public static class productViewholder extends RecyclerView.ViewHolder {
        TextView pname, taskType;
        CheckBox check;
        CardView cardView;
        ImageView more, barcode, locate;
        LinearLayout productwithcheckbox_item;
        //TextView counter;

        public productViewholder(View itemView) {
            super(itemView);
            itemView.setClickable(true);
            pname = (TextView) itemView.findViewById(R.id.productname);
            check = (CheckBox) itemView.findViewById(R.id.checkbox);
            cardView = (CardView) itemView.findViewById(R.id.cardview);
            more = (ImageView) itemView.findViewById(R.id.more);
            barcode = (ImageView) itemView.findViewById(R.id.barcode);
            locate = (ImageView) itemView.findViewById(R.id.locate);
            productwithcheckbox_item = (LinearLayout) itemView.findViewById(R.id.productwithcheckbox_item);
            taskType = (TextView) itemView.findViewById(R.id.taskType);
        }
    }

    public void UpdateOrderTask(String taskid, final String type) {
        System.out.println("@@## UpdateOrderTask Called");
        System.out.println("@@## type" + type);


        String url = "";
//                Constants.updateAssociateTaskURL + Integer.parseInt(taskid);

        StringRequest reqpatch = new StringRequest(Request.Method.PATCH, "http://rmethapi.azurewebsites.net/api/JDAServices/UpdateTaskAPI?taskId=" + Integer.parseInt(taskid), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                System.out.println("@@## inbound:%n %s" + response);

//                    reply=response;
//                Toast.makeText(context, response, Toast.LENGTH_LONG).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("Error: " + error.getMessage());
//                callCount = 0;
//                    reply="server not working";
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("status", type);
                return params;
            }

            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                int mStatusCode = response.statusCode;
                Log.d("@@##", "Response Back " + response.statusCode);
                if (mStatusCode == HttpURLConnection.HTTP_OK) {
                } else {
//                    callCount = 0;
                }
                return super.parseNetworkResponse(response);
            }
        };

        HttpStack httpStack = new HttpClientStack(AndroidHttpClient.newInstance("volley/0"));
        mRequestQueue = Volley.newRequestQueue(context, httpStack);
        mRequestQueue.add(reqpatch);
    }


}
