package com.cognizant.retailamate.jda.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.cognizant.retailamate.jda.R;
import com.cognizant.retailamate.jda.utils.AccountState;

public class SplashActivity extends AppCompatActivity {
    private static final String TAG = SplashActivity.class.getSimpleName();
    public static boolean associateServiceReady = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.splash_activity);
        Log.d(TAG, "onCreate Splash Activity");

        try {
            if (AccountState.getPrefBeaconId().equals(null)) {

            }
        } catch (NullPointerException e) {
            AccountState.setPrefBeaconId("id_token");
        }

        Thread thread = new Thread() {
            @Override
            public void run() {
                try {
                    synchronized (this) {
                        wait(2000);
                    }
                } catch (InterruptedException ex) {
                }
                Intent i = new Intent();
                i.setClass(SplashActivity.this, LoginActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                finish();
            }
        };
        thread.start();
    }
}
