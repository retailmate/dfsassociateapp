package com.cognizant.retailamate.jda.buddystore.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.cognizant.retailamate.jda.R;
import com.cognizant.retailamate.jda.buddystore.adapter.ProductAdapter;
import com.cognizant.retailamate.jda.buddystore.interfaces.ClearInterface;
import com.cognizant.retailamate.jda.buddystore.interfaces.ProductInterface;
import com.cognizant.retailamate.jda.buddystore.model.Product;
import com.cognizant.retailamate.jda.buddystore.popup.RequestModePopUp;
import com.cognizant.retailamate.jda.utils.Constants;
import com.google.gson.Gson;

import java.util.ArrayList;

public class RMProductScreen extends AppCompatActivity implements View.OnClickListener, ProductInterface, ClearInterface {

    private static final int RESULT_CLOSE_ALL = 0;
    private Gson gson;
    public static Product product;
    private ArrayList<Product.VariantsBean> mProductArrayList;
    private RecyclerView mRecyclerView;
    private ProductAdapter mAdapter;
    private Context mContext;
    Button requestBuddyStoreButton;
    int productId;
    ImageView imageView;

    TextView storeName, productName, productIdtv, productCategory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_details_screen);

        productId = getIntent().getIntExtra("productID", 0);

        requestBuddyStoreButton = (Button) findViewById(R.id.check_buddy_store_button);

        requestBuddyStoreButton.setOnClickListener(this);
        imageView = (ImageView) findViewById(R.id.image_view);
        storeName = (TextView) findViewById(R.id.store_name);
        productName = (TextView) findViewById(R.id.product_name);
        productIdtv = (TextView) findViewById(R.id.product_id);
        productCategory = (TextView) findViewById(R.id.product_category);


        if (Constants.productId == 157760929)
            imageView.setImageResource(R.mipmap.ic_157760929);
        else if (Constants.productId == 156859972)
            imageView.setImageResource(R.mipmap.ic_156859972);
        else if (Constants.productId == 122092927)
            imageView.setImageResource(R.mipmap.ic_122092927);

        mContext = getApplicationContext();

        gson = new Gson();

        mProductArrayList = new ArrayList<>();


        String responseJson = "response_" + productId;

        Log.e("@@##", responseJson);

        product = gson.fromJson(Constants.loadJSONFromAsset(responseJson, mContext), Product.class);


        storeName.setText(product.getStore());
        productName.setText(product.getName());
        productIdtv.setText(product.getID());
        productCategory.setText(product.getCategory());

        for (int i = 0; i < product.getVariants().size(); i++) {
            mProductArrayList.add(product.getVariants().get(i));
        }

        mRecyclerView = (RecyclerView) findViewById(R.id.product_info_recycler_view);

        mAdapter = new ProductAdapter(mProductArrayList, mContext, this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mAdapter);


    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.check_buddy_store_button:
                Intent intent = new Intent(mContext, RequestModePopUp.class);
                intent.putExtra("position", ProductAdapter.lastCheckedPos);
                startActivityForResult(intent, RESULT_CLOSE_ALL);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (resultCode) {
            case RESULT_CLOSE_ALL:
                setResult(RESULT_CLOSE_ALL);

                finish();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void enableButton() {
        requestBuddyStoreButton.setEnabled(true);
    }

    @Override
    public void disableButton() {
        requestBuddyStoreButton.setEnabled(false);
    }

    @Override
    public void setIfDisabled() {
        if (!requestBuddyStoreButton.isEnabled()) {
            requestBuddyStoreButton.setEnabled(true);
        }
    }

    @Override
    public void clearActivity() {
        finish();
    }
}
