package com.cognizant.retailamate.jda.Model;

import java.util.List;

/**
 * Created by 452781 on 11/25/2016.
 */
public class AssociateJobModelold {


    /**
     * type : pick
     * quantity : 3
     * itemName : LEE COOPER Mens Leather Lace Up Casual Shoe
     * location : 987654321
     */

    private List<ResourceBean> resource;

    public List<ResourceBean> getResource() {
        return resource;
    }

    public void setResource(List<ResourceBean> resource) {
        this.resource = resource;
    }

    public static class ResourceBean {
        private String type;
        private int quantity;
        private String itemName;
        private String location;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public int getQuantity() {
            return quantity;
        }

        public void setQuantity(int quantity) {
            this.quantity = quantity;
        }

        public String getItemName() {
            return itemName;
        }

        public void setItemName(String itemName) {
            this.itemName = itemName;
        }

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }
    }
}
